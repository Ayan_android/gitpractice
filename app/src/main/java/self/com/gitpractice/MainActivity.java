package self.com.gitpractice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toast.makeText(getApplicationContext(), "Hello", Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "Hello", Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "Hi", Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "Hey HI", Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "Matser brach", Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "Debug brach", Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "New Debug branch", Toast.LENGTH_SHORT).show();
        Toast.makeText(getApplicationContext(), "New NewDebug branch", Toast.LENGTH_SHORT).show();
    }
}
